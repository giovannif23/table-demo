This is a small demo project that was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app), to show the table select functionality.

## Developement environment 

```sh
Node v6.2.2
npm 3.9.5
yarn 1.6.0
```

## Setup

### 1. Get the source code 

```sh
$ git clone -b master git@bitbucket.org:giovannif23/table_demo.git table-demo
$ cd table-demo
```

### 2. Install dependencies

```sh
$ npm install
```

### 3. Run the app

```sh
$ npm start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### 3. Run the tests

```sh
$ npm test
```

Currently will only tests app initialization.

## Development

This demo was created using the [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) principles and [Styled Components](https://www.styled-components.com/) with [Polished](https://polished.js.org/).
