import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { mix } from 'polished';
import theme from '../theme';

const StyledAlertBox = styled.div`
  background-color: ${mix(.85, theme.palette.white, theme.palette.blue)};
  border-collapse: collapse; 
  border-radius: 7px;
  box-sizing: border-box;
  color: ${theme.palette.grey.dark};
  font-size: 13px;
  height: auto;
  margin: 0 auto 20px;
  padding: 30px 30px 20px;
  position: relative;
  overflow: hidden;
  text-align: left;
  width: 100%;

  h1 {
    font-size: 17px;
    margin: 0;
  }

  ol {
    list-style-position: inside;
    margin: 0;
    padding: 10px;
  }

  li {
    line-height: 1.5;
    padding: 0;
  }

  button {
    background-color: transparent;
    border: 0;
    border-radius: 5px;
    color: ${theme.palette.grey.dark};
    height: 26px;
    padding: 5px 7px;
    position: absolute;
    right: 15px;
    text-align: center;
    top: 15px;
    transition: background-color .1s, color .1s ease-in-out;
    width: auto;

    &:hover {
      background-color: ${mix(.75, theme.palette.white, theme.palette.blue)};
      color: ${theme.palette.blue};
      cursor: pointer;
    }
  }
`;

const AlertBox = ({ data, closeHandler, ...props }) => {
  return (
    <StyledAlertBox {...props}>
      <h1>You have selected to download the following files:</h1>
      <ol>
        {data.map((path, index) => (
          <li key={index}>{path}</li>
        ))}
      </ol>   
      <button onClick={() => closeHandler()}>Close</button>
    </StyledAlertBox>
  );
};

AlertBox.propTypes = {
  data: PropTypes.array,
  closeHandler: PropTypes.func,
};

export default AlertBox;
