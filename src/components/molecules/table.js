import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { rgba } from 'polished';
import { capitalize } from '../../utils/helpers';
import theme from '../theme';
import DownloadIcon from '../../assets/icon_download';
import AlertBox from '../molecules/alertbox';
import { TableRow, TableRowAction } from '../atoms/tablerow';
import TableCell from '../atoms/tablecell';

const TableWrap = styled.div`
  box-sizing: border-box;
  height: auto;
  margin: 0 auto;
  max-width: 950px;
  width: 100%;
`;

const StyledTable = styled.table`
  background-color: ${theme.palette.white};
  border-collapse: collapse; 
  border: 1px solid ${theme.palette.grey.medium};
  border-radius: 7px;
  box-shadow: 0 0 0 1px ${rgba(theme.palette.grey.medium, 1)};
  box-sizing: border-box;
  color: ${theme.palette.grey.dark};
  height: auto;
  margin: 0 auto;
  overflow: hidden;
  text-align: center;
  width: 100%;
`;

class Table extends Component {
  state = {
    data: this.props.data,
    selected: {},
    selectAll: 0,
    showAlert: false,
  };

  downloadFiles = () => {
    this.setState({
      showAlert: true,
    });
  };

  closeAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  selectRow(path) {
    const newObj = Object.assign({}, this.state.selected);
    const isSelected = !this.state.selected[path];

    if (isSelected) {
      newObj[path] = isSelected;
    } else {
      delete newObj[path];
    }

    const selectAllState = () => {
      if (this.state.data.length === Object.keys(newObj).length) {
        return 1; // all are selected
      }
      if (Object.keys(newObj).length === 0) {
        return 0; // no items selected
      }
      return 2; // some items selected
    };

    this.setState({
      selected: newObj,
      selectAll: selectAllState(),
      showAlert: false,
    });
  };

  selectAllRows = () => {
    const newObj = {};
    if (!this.state.selectAll) {
      this.state.data.map((item, index) => newObj[item.path] = true);
    }

    this.setState({
      selected: newObj,
      selectAll: !this.state.selectAll ? 1 : 0,
      showAlert: false,
    });
  };

  render() {
    const { selected, showAlert } = this.state;
    const selectedText = Object.keys(selected).length ? `Selected ${Object.keys(selected).length}` : 'None Selected';
    const selectedRowsArray = Object.entries(selected);
    
    return (
      <TableWrap>
        {showAlert && <AlertBox closeHandler={this.closeAlert} data={selectedRowsArray}></AlertBox>}
        <StyledTable>
          <tbody>
            <TableRowAction>
              <TableCell colSpan="5">
                <input
                  name="selectAll"
                  type="checkbox"
                  checked={this.state.selectAll === 1}
                  onChange={this.selectAllRows}
                  ref={input => {
                    if (input) {
                      input.indeterminate = this.state.selectAll === 2;
                    }
                  }} />
                <span>{selectedText}</span>
                <button disabled={!selectedRowsArray.length} onClick={() => this.downloadFiles()} type="button">
                  <DownloadIcon /> Download Selected
                  </button>
              </TableCell>
            </TableRowAction>
            <TableRow>
              <TableCell heading></TableCell>
              <TableCell heading>Name</TableCell>
              <TableCell heading>Device</TableCell>
              <TableCell heading>Path</TableCell>
              <TableCell heading>Status</TableCell>
            </TableRow>
            {this.state.data.map((data, index) => (
              <TableRow className={this.state.selected[data.path] === true ? 'active' : ''} key={index}>
                <TableCell>
                  <input
                    name={data.name}
                    type="checkbox"
                    checked={this.state.selected[data.path] === true}
                    onChange={() => this.selectRow(data.path)} />
                </TableCell>
                <TableCell>{data.name}</TableCell>
                <TableCell>{data.device}</TableCell>
                <TableCell>{data.path}</TableCell>
                <TableCell status={data.status}>{capitalize(data.status)}</TableCell>
              </TableRow>
            ))}
          </tbody>
        </StyledTable>
      </TableWrap>
    );
  }
};

Table.propTypes = {
  data: PropTypes.array,
};

export default Table;
