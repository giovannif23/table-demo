import '../../utils/jestsetup';
import React from 'react';
import ReactDOM from 'react-dom';
import Table from './Table';

describe("the Table initializes correctly", () => {
  const mockData = [];
  const wrapper = shallow(<Table data={mockData} />);

  it('it has a method to download files', () => {
    wrapper.instance().downloadFiles();
  });

  it('it has a method to close the alert box', () => {
    wrapper.instance().closeAlert();
  });

  it('it has a method to select a row', () => {
    wrapper.instance().selectRow();
  });

  it('it has a method to select all rows', () => {
    wrapper.instance().selectAllRows();
  });
});