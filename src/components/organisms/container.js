import styled from 'styled-components';
import theme from '../theme';

const Container = styled.div`
  align-items: center;
  background: linear-gradient(-45deg, ${theme.palette.grey.medium}, ${theme.palette.white});
  box-sizing: border-box;
  color: ${theme.palette.grey.dark};
  display: flex;
  height: 100vh;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  width: 100%;
`;

export default Container;
