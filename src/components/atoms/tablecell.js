import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import theme from '../theme';

const styles = css`
  text-align: left;
  padding: 15px;;
`;

const Th = styled.th`
  ${styles};
  font-family: ${theme.fonts.primary};
  font-size: 14px;
  font-weight: 300;
  letter-spacing: -.1ex;
`;

const Td = styled.td`
  ${styles};
  font-size: 11px;
  position: relative;
  
  &:before {
    display: ${props => props.status === 'available' ? 'block' : 'none'};
    position: absolute;
    content: '';
    width: 16px;
    height: 16px;
    background-color: ${theme.palette.green};
    border-radius: 50%;
    left: -10px;
    top: 50%;
    margin-top: -8px;
  }

  input[type="checkbox"] {
    &:before {
      background: ${theme.palette.white};
      border: 2px solid ${theme.palette.grey.medium};
      border-radius: 2px;
      content: '';
      display: inline-block;
      height: 14px;
      line-height: 14px;
      margin-left: -2px;
      margin-top: -2px;
      width: 14px;
    }

    &:indeterminate:before {
      background-color: ${theme.palette.blue};
      background-image: url('../assets/icon_dash.svg');
      background-size: 10px 3px;
      background-repeat: no-repeat;
      background-position: center;
      border-color: ${theme.palette.blue};
    }

    &:checked:before {
      background-color: ${theme.palette.blue};
      background-image: url('../assets/icon_check.svg');
      background-size: 13px 10px;
      background-repeat: no-repeat;
      background-position: center 2px;
      border-color: ${theme.palette.blue};
    }
  }
`;

const TableCell = ({ heading, children, ...props }) => {
  return React.createElement(heading ? Th : Td, props, children);
};

TableCell.propTypes = {
  heading: PropTypes.bool,
  children: PropTypes.any,
};

export default TableCell;