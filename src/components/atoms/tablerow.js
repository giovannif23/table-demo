import styled from 'styled-components';
import { mix } from 'polished';
import theme from '../theme';

export const TableRow = styled.tr`
  background-color: ${theme.palette.white};
  border-bottom: 1px solid ${theme.palette.grey.medium};

  &:hover {
    background-color: ${theme.palette.grey.light};
  }

  &.active {
    background-color: ${theme.palette.grey.medium};
  }
`;

export const TableRowAction = TableRow.extend`
  &:hover {
    background-color: transparent;
  }

  td {
    font-size: 17px;
    letter-spacing: -.1ex;
    padding-bottom: 10px;
    padding-top: 20px;
  }

  input[type="checkbox"] {
    margin-right: 20px;
  }

  span {
    display: inline-block;
    width: 130px;
  }

  button {
    border: 0;
    background-color: ${theme.palette.white};
    border-radius: 4px;
    color: ${theme.palette.grey.dark};
    cursor: pointer;
    letter-spacing: -.1ex;
    padding: 7px 10px;
    transition: color .1s, background-color .1s ease-in-out;

    &:hover,
    &:focus {
      color: ${theme.palette.blue};
      background-color: ${mix(.85, theme.palette.white, theme.palette.blue)};

      svg {
        path {
          fill: ${theme.palette.blue};
        }
      }
    }

    &:disabled {
      background-color: transparent;
      color: ${theme.palette.grey.mediumDark};
      cursor: not-allowed;

      svg {
        path {
          fill: ${theme.palette.grey.mediumDark};
        }
      }
    }

    svg {
      margin-right: 5px;

      path {
        fill: ${theme.palette.grey.dark};
      }
    }
  }
`;
