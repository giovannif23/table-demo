const theme = {};

theme.palette = {
  blue: '#4f94dd',
  green: '#85ce3f',
  black: '#000',
  white: '#fff',
  grey: {
    light: '#f5f5f5',
    medium: '#eeeeee',
    mediumDark: '#b3b3b3',
    dark: '#4b4b4b',
  }
};

theme.fonts = {  
  primary: `'Roboto', sans-serif`
};

export default theme;