import React from 'react';
import Container from './components/organisms/container';
import Table from './components/molecules/table';
import data from './utils/db';

const App = () => {
  return (
    <Container>
      <Table data={data} />
    </Container>
  );
}

export default App;
